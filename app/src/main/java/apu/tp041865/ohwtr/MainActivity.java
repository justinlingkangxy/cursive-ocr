package apu.tp041865.ohwtr;

import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.googlecode.tesseract.android.TessBaseAPI;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText editText;
    private CropImageView cropImageView;

    private boolean HAS_IMG = false;
    private String DATA_PATH = "";
    private String lang = "";
    private String typed_text = "Typed text";
    private String handwritten_text = "Handwritten text";

    private TessBaseAPI mTess;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {

                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.navigation_load:
                            boolean result = Utility.checkPermission(MainActivity.this);
                            if (result)
                                loadImage();
                            return true;
                        case R.id.navigation_scan:
                            if (HAS_IMG)
                                promptTask();
                            else
                                Toast.makeText(getApplicationContext(), "No image detected! Please load a image.", Toast.LENGTH_LONG).show();
                            return true;
                        case R.id.navigation_save:
                            if (!editText.getText().toString().equals(""))
                                promptDialog();
                            else
                                Toast.makeText(getApplicationContext(), "No result yet.", Toast.LENGTH_LONG).show();
                            return true;
                    }
                    return false;

                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        editText = (EditText) findViewById(R.id.editText);
        editText.setFocusable(false);
        editText.setFocusableInTouchMode(false);
        editText.setClickable(false);

        cropImageView = (CropImageView) findViewById(R.id.cropImageView);
    }

    /**
     * Create customize menu bar.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_toolbar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Set customize menu bar action.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.tool_rotate_left:
                if (HAS_IMG)
                    rotateLeft();
                else
                    Toast.makeText(getApplicationContext(), "No image detected! Please load a image.", Toast.LENGTH_LONG).show();
                return true;
            case R.id.tool_rotate_right:
                if (HAS_IMG)
                    rotateRight();
                else
                    Toast.makeText(getApplicationContext(), "No image detected! Please load a image.", Toast.LENGTH_LONG).show();
                return true;
            case R.id.tool_crop:
                if (HAS_IMG)
                    cropImage();
                else
                    Toast.makeText(getApplicationContext(), "No image detected! Please load a image.", Toast.LENGTH_LONG).show();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Check permission.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loadImage();
                } else {
                    Toast.makeText(this, "Required permissions are not granted", Toast.LENGTH_LONG).show();
                }
                break;
            case Utility.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    promptDialog();
                } else {
                    Toast.makeText(this, "Required permissions are not granted", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    /**
     * On crop button click, start crop the image and set it back to the cropping view.
     */
    public void cropImage() {
        cropImageView.setImageBitmap(cropImageView.getCroppedImage());
    }

    /**
     * On rotate left button click, start rotate the image 90 degree counter-clockwise and set it back to the cropping view.
     */
    public void rotateLeft() {
        cropImageView.rotateImage(-90);
        cropImageView.setImageBitmap(cropImageView.getCroppedImage());
    }

    /**
     * On rotate button click, start rotate the image 90 degree clockwise and set it back to the cropping view.
     */
    public void rotateRight() {
        cropImageView.rotateImage(90);
        cropImageView.setImageBitmap(cropImageView.getCroppedImage());
    }

    /**
     * Let user choose the text type (Typed text or Handwritten text).
     */
    public void promptTask() {
        // initialize Tesseract API
        DATA_PATH = getFilesDir() + "/tesseract/";
        mTess = new TessBaseAPI();

        final CharSequence[] items = {typed_text, handwritten_text, "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Text in the image is:");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals(typed_text)) {
                    // initialize Tesseract API
                    lang = "typed_eng";
                    checkFile(new File(DATA_PATH + "tessdata/"));
                    mTess.init(DATA_PATH, lang);

                    // binarize the image before perform OCR
                    doOCR(binarizeImage(cropImageView.getCroppedImage()));
                } else if (items[item].equals(handwritten_text)) {
                    // initialize Tesseract API
                    lang = "handwrite_eng";
                    checkFile(new File(DATA_PATH + "tessdata/"));
                    mTess.init(DATA_PATH, lang);

                    // binarize the image before perform OCR
                    doOCR(binarizeImage(cropImageView.getCroppedImage()));
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /**
     * Check traineddata file.
     */
    private void checkFile(File dir) {
        if (!dir.exists() && dir.mkdirs()) {
            copyFiles();
        }
        if (dir.exists()) {
            String datafilepath = DATA_PATH + "/tessdata/" + lang + ".traineddata";
            File datafile = new File(datafilepath);

            if (!datafile.exists()) {
                copyFiles();
            }
        }
    }

    /**
     * Copy traineddata file.
     */
    private void copyFiles() {
        try {
            String filepath = DATA_PATH + "/tessdata/" + lang + ".traineddata";
            AssetManager assetManager = getAssets();

            InputStream instream = assetManager.open("tessdata/" + lang + ".traineddata");
            OutputStream outstream = new FileOutputStream(filepath);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = instream.read(buffer)) != -1) {
                outstream.write(buffer, 0, read);
            }

            outstream.flush();
            outstream.close();
            instream.close();

            File file = new File(filepath);
            if (!file.exists()) {
                throw new FileNotFoundException();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Active the OCR.
     */
    public void doOCR(final Bitmap bitmap) {
        mTess.setImage(bitmap);

        String OCRresult = mTess.getUTF8Text();

        editText.setText(OCRresult);
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.setClickable(true);
    }

    /**
     * Binarize the image.
     */
    private Bitmap binarizeImage(Bitmap orginalBitmap) {
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0);

        ColorMatrixColorFilter colorMatrixFilter = new ColorMatrixColorFilter(colorMatrix);

        Bitmap binarizedBitmap = orginalBitmap.copy(Bitmap.Config.ARGB_8888, true);

        Paint paint = new Paint();
        paint.setColorFilter(colorMatrixFilter);

        Canvas canvas = new Canvas(binarizedBitmap);
        canvas.drawBitmap(binarizedBitmap, 0, 0, paint);

        cropImageView.setImageBitmap(binarizedBitmap);

        return binarizedBitmap;
    }

    /**
     * On load image button click, start pick image activity.
     */
    public void loadImage() {
        startActivityForResult(getPickImageChooserIntent(), 200);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Set image to cropImageView.
        if (resultCode == RESULT_OK) {
            Uri imageUri = getPickImageResultUri(data);
            cropImageView.setImageUriAsync(imageUri);
            HAS_IMG = true;
        }
    }

    /**
     * Create a chooser intent to select the  source to get image from.
     * The source can be either from camera (ACTION_IMAGE_CAPTURE) or gallery (ACTION_GET_CONTENT).
     * All possible sources are added to the  intent chooser.
     */
    public Intent getPickImageChooserIntent() {

        // determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // include the main intent
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    /**
     * Get the URI of the selected image from getPickImageChooserIntent().
     * Will return the correct URI for camera and gallery image.
     */
    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    /**
     * Prompt dialog for user to input file name
     */
    public void promptDialog() {
        // get prompt.xml view
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.prompt, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set prompt.xml to alert dialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText fileName = (EditText) promptsView.findViewById(R.id.filename);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // get user input and save file
                                if (fileName.getText().toString().equals("")) {
                                    Toast.makeText(getApplicationContext(), "File name must not be empty!", Toast.LENGTH_LONG).show();
                                } else {
                                    saveResult(fileName.getText().toString());
                                }
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    /**
     * Save the result into the text file
     */
    public void saveResult(String filename) {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            File docFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);

            File filedoc;
            FileOutputStream out;
            String ocrtxt = editText.getText().toString();
            try {
                // will overwrite existing file if with same name
                filedoc = new File(docFolder, filename + ".txt");

                out = new FileOutputStream(filedoc);
                out.write(ocrtxt.getBytes());
                out.close();
                Toast.makeText(this, "The contents are saved in " + filedoc, Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(this, "Exception: " + e.toString(), Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "The external disk is not mounted!", Toast.LENGTH_LONG).show();
        }
    }
}
